const express = require("express");
const app = express();

const port = process.env.port || 3000;
app.get("/api/demo", (req, res) => {
  res.json({ data: "I am Okay!!" });
});
app.listen(port, () => console.log(`App is listen in port ${port}`));
