FROM node:12-stretch-slim AS builder
WORKDIR /usr/src/app/backend
COPY /backend/package*.json ./

RUN npm install
COPY . .

EXPOSE 3000
CMD [ "node", "backend/index.js" ]